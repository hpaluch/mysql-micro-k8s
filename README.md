# Deploying WordPress + MySQL to Kubernetes - MicroK8s + OpenEBS

Deploying WordPress and MySQL to K8s with OpenEBS replicated volumes.
We use 2 Volumes:

1. /var/lib/mysql for MySQL backend
1. /var/www/html - for WordPress website, plugins and uploads

Tested Kubernetes environment:

- MicroK8s implementation of Kubernetes:
  - see https://microk8s.io/
- at least 3 nodes required
- must have configured and enabled OpenEBS plugin with Jiva CSI backend
  - see https://github.com/openebs/jiva-operator/blob/develop/docs/quickstart.md
- Node configuration:
  - OS: `Ubuntu 20.04.3 LTS`
  - HW: `Standard B2s (2 vcpus, 4 GiB memory)`
  - Disk 32GB `Standard HDD`
  - Private Network only
- containers snap versions:

  ```bash
  $ microk8s kubectl get nodes

  NAME      STATUS   ROLES    AGE   VERSION
  k8s-vm3   Ready    <none>   43h   v1.22.2-3+9ad9ee77396805
  k8s-vm4   Ready    <none>   48m   v1.22.2-3+9ad9ee77396805
  k8s-vm1   Ready    <none>   43h   v1.22.2-3+9ad9ee77396805
  k8s-vm2   Ready    <none>   43h   v1.22.2-3+9ad9ee77396805
  ```

  (yes - I have 4 nodes instead of required 3 - I plan to test failover soon)
- I have these plugins enabled:

  ```bash
  $ microk8s status | sed '/disabled:/,$d'

  microk8s is running
  high-availability: yes
    datastore master nodes: 10.101.0.6:19001 10.101.0.4:19001 10.101.0.7:19001
    datastore standby nodes: 10.101.0.8:19001
  addons:
    enabled:
    dns                  # CoreDNS
    ha-cluster           # Configure high availability on the current node
    helm3                # Helm 3 - Kubernetes package manager
    ingress              # Ingress controller for external access
    openebs              # OpenEBS is the open-source storage solution for Kubernetes
    rbac                 # Role-Based Access Control for authorisation
    storage              # Storage class; allocates storage from host directory
  ```

- ensure that Jiva CSI storage class exist in your
  environment (otherwise your OpenEBS is not correctly configured):

  ```bash
  $ microk8s.kubectl get sc openebs-jiva-csi-default

  NAME                       PROVISIONER           RECLAIMPOLICY   VOLUMEBINDINGMODE   ALLOWVOLUMEEXPANSION   AGE
  openebs-jiva-csi-default   jiva.csi.openebs.io   Delete          Immediate           true                   43h
  ```

## General setup

I have made SNAP alias:

```bash
sudo snap alias microk8s.kubectl kubectl
```

So now we can invoke just `kubectl` instead
of `microk8s.kubectl` or `microk8s kubectl`.

## How to setup MySQL on OpenEBS/Jiva

We will mostly use this guide:

- https://openebs.io/docs/user-guides/jiva-guide#provision-sample-applications-with-jiva-volume

> NOTE: I plan to use this DB for WordPress example - so the names...

At first create namespace `wp-demo-ns` (we will atempt to create all resources there):

```bash
kubectl create namespace wp-demo-ns
```

Create bash alias for this namespace:

```bash
alias k-wp='kubectl -n wp-demo-ns'
```

And put above line to the end of your `~/.bashrc`

Install openssl to get password generator:

```bash
sudo apt-get install openssl
```

Now generate passwords:

```bash
# removed weird characters from password to avoid confusion
# MUST remove EOL - \n with tr !!!
openssl rand -base64 16 | tr -d '+=/\n' > ~/.mysql_root_password.txt
openssl rand -base64 16 | tr -d '+=/\n' > ~/.mysql_wp_password.txt
```

Use this password to create two  `Secret` objects holding passwords:

```bash
k-wp create secret generic mysql-root-sec \
   --from-file=password=$HOME/.mysql_root_password.txt
k-wp create secret generic mysql-wp-sec \
   --from-file=password=$HOME/.mysql_wp_password.txt
```

Verification can be done with:

```bash
$ k-wp get secrets

NAME                  TYPE                                  DATA   AGE
default-token-kjwbv   kubernetes.io/service-account-token   3      18m
mysql-root-sec        Opaque                                1      44s
mysql-wp-sec          Opaque                                1      20s
```

(Not sure what is that `default-token-kjwbv`).

To extract password back you can use this command:

```bash
# MySQL Root password
k-wp get secret mysql-root-sec -o jsonpath='{.data.password}' | \
  base64 --decode

# MySQL wp (wordpress) password
k-wp get secret mysql-wp-sec -o jsonpath='{.data.password}' | \
  base64 --decode
```

Double-check that there is no trailing newline after password (I spent
many hours trying to connect to such MySQL server....)

Now create Persistent Volume Claim (PVC) for MySQL:

```bash
$ k-wp apply -f manifests/mysql-pvc.yaml

persistentvolumeclaim/wp-mysql-pvc created
```

Verify that PVC was created and STATUS is `Bound`:

```bash
$ k-wp get pvc

NAME           STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS               AGE
wp-mysql-pvc   Bound    pvc-ebd0fd47-2332-4df3-a999-61e8f07b1f6e   5G         RWO            openebs-jiva-csi-default   2m38s
```

Now crate MySQL Deployment using:

```bash
$ k-wp apply -f manifests/mysql-deploy.yaml

deployment.apps/wp-mysql-dep created
```

Verify with:

```bash
$ k-wp get deploy

NAME           READY   UP-TO-DATE   AVAILABLE   AGE
wp-mysql-dep   0/1     1            0           17s
```

Wait for a while and try again until
READY=`1/1` and AVAILABLE=`1`, for example:

```bash
$ k-wp get deploy

NAME           READY   UP-TO-DATE   AVAILABLE   AGE
wp-mysql-dep   1/1     1            1           5m10s
```

Now we have to create service so we can reach our MySQL using DNS name:

```bash
$ k-wp apply -f manifests/mysql-svc.yaml

service/wp-mysql-svc created

$ k-wp get svc

NAME           TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
wp-mysql-svc   ClusterIP   None         <none>        3306/TCP   14s
```

The `wp-mysql-svc` service is accessible from Kubernetes Cluster ONLY!
To access it we may nearly follow:

- https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

```bash
$ k-wp run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- \
    mysql -h wp-mysql-svc -u wp -p`cat ~/.mysql_wp_password.txt` wp_db

If you don't see a command prompt, try pressing enter.
### press <ENTER>
mysql> show tables;
Empty set (0.00 sec)

mysql> quit
Bye
### press Ctrl-C
```

Now you have to manually drop pod `mysl-client`

```bash
$ k-wp delete pod mysql-client

pod "mysql-client" deleted
```

## Deploying WordPress Web application

To test MySQL we will deploy official WordPress Docker application.

At first create PVC `wp-html-pvc` to hold WordPress HTML and uploaded
content:

```bash
$ k-wp apply -f manifests/wordpress-pvc.yaml

persistentvolumeclaim/wp-html-pvc created
```

Ensure that both Volumes (MySQL and HTML) exists and
that Acess Mode is `RWO` (Read-Write-Once):

```bash
$ k-wp get pvc

NAME           STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS               AGE
wp-mysql-pvc   Bound    pvc-ebd0fd47-2332-4df3-a999-61e8f07b1f6e   5G         RWO            openebs-jiva-csi-default   158m
wp-html-pvc    Bound    pvc-f8d7449d-0d61-4c5f-ad3e-fb76dfc6947e   5G         RWO            openebs-jiva-csi-default   69s
```

Now create WordPress deployment:

```bash
$ k-wp apply -f manifests/wordpress-deploy.yaml

deployment.apps/wp-html-dep created
```

Ater a while check numbe of deployments - must be `1/1`

```bash
$ k-wp get deploy wp-html-dep

NAME          READY   UP-TO-DATE   AVAILABLE   AGE
wp-html-dep   1/1     1            1           87s
```

To verify that WordPerfect Web Deployment works (we did not
exposed it yet) try this:

```bash
# Get Pod name
$ k-wp get pod -l app=wp-html

NAME                           READY   STATUS    RESTARTS   AGE
wp-html-dep-68ccc9c76f-smd7b   1/1     Running   0          108s

# use above Pod name in this command:
$ k-wp exec wp-html-dep-68ccc9c76f-smd7b -- /usr/bin/curl -fsSL http://127.0.0.1
```

You should see redirect and lot of wordpress HTML/javascript code...

WARNING! Above curl step is important - because even when Pod is running there
could be fatal error reported (for example on Databse connection error).

To expose our wordpress we have to

1. create Service object
1. create Ingress object referencing Service object

To create Service object, run:

```bash
$ k-wp apply -f manifests/wordpress-svc.yaml

service/wp-html-svc created

$ k-wp get svc wp-html-svc -o wide

NAME          TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE   SELECTOR
wp-html-svc   ClusterIP   None         <none>        80/TCP    43s   app=wp-html
```

Finally we have to create Ingress (basically
Nginx frontend with Virtual Host)
to access WordPress Service

To create Ingress object run:

```bash
$ k-wp apply -f manifests/wordpress-ingress.yaml

ingress.networking.k8s.io/wp-ingress created
```

After a while query status of Ingress:

```bash
$ k-wp get ingress

NAME         CLASS    HOSTS                ADDRESS     PORTS   AGE
wp-ingress   public   wp-k8s.example.com   127.0.0.1   80      30s
```

Now you have to add to your `/etc/hosts` or `c:\Windows\System32\drivers\etc\hosts`:

```
IP_OF_ANY_NODE wp-k8s.example.com
```

And try to fire-up http://wp-k8s.example.com in your browser.

You should see WordPress installation page :-)


## Failover (HA) in practice

I deployed this project (WordPress + MySQL) to 4 node MicroK8s instances.
Once everything worked I decided to test HA  - Failover (3 running nodes
are minimum for HA and for 3 Volume replicas to be ReadWrite).

At first I looked up, where is WordPress and/or MySQL running:

```bash
$ kubectl get pods -n wp-demo-ns -o wide

NAME                            READY   STATUS    RESTARTS   AGE     IP            NODE      NOMINATED NODE   READINESS GATES
wp-mysql-dep-678d654858-cdnfg   1/1     Running   0          3h35m   10.1.232.5    k8s-vm4   <none>           <none>
wp-html-dep-68ccc9c76f-smd7b    1/1     Running   0          80m     10.1.232.16   k8s-vm4   <none>           <none>
```

Both were running on node `k8s-vm4`.

I logged in Azure Portal and Stopped Vm instance `k8s-vm4`.
After a while I verified thet K8s noticed that `k8s-vm4` is not available:

```bash
$ kubectl get nodes

NAME      STATUS     ROLES    AGE     VERSION
k8s-vm1   Ready      <none>   2d      v1.22.2-3+9ad9ee77396805
k8s-vm2   Ready      <none>   2d      v1.22.2-3+9ad9ee77396805
k8s-vm3   Ready      <none>   2d      v1.22.2-3+9ad9ee77396805
k8s-vm4   NotReady   <none>   5h53m   v1.22.2-3+9ad9ee77396805
```

Notice STATUS=`NotReady` for `k8s-vm4`.

Then I got state of my Deployments:

```bash
$ kubectl get deployment -n wp-demo-ns

NAME           READY   UP-TO-DATE   AVAILABLE   AGE
wp-mysql-dep   0/1     1            0           3h39m
wp-html-dep    0/1     1            0           84m
```

Now I became a bit scared - because it looked like nothing was happening
(WordPresss was offline as expected). However after around 5 minutes
it was finally restarted on Existing nodes and worked again without problems.

Here is event list that clearly describe what happened:

```bash
$ k-wp get events

LAST SEEN   TYPE      REASON                 OBJECT                               MESSAGE
10m         Warning   NodeNotReady           pod/wp-mysql-dep-678d654858-cdnfg    Node is not ready
10m         Warning   NodeNotReady           pod/wp-html-dep-68ccc9c76f-smd7b     Node is not ready
5m16s       Normal    TaintManagerEviction   pod/wp-html-dep-68ccc9c76f-smd7b     Marking for deletion Pod wp-demo-ns/wp-html-dep-68ccc9c76f-smd7b
5m16s       Normal    TaintManagerEviction   pod/wp-mysql-dep-678d654858-cdnfg    Marking for deletion Pod wp-demo-ns/wp-mysql-dep-678d654858-cdnfg
5m16s       Normal    SuccessfulCreate       replicaset/wp-mysql-dep-678d654858   Created pod: wp-mysql-dep-678d654858-tc2l6
5m16s       Normal    SuccessfulCreate       replicaset/wp-html-dep-68ccc9c76f    Created pod: wp-html-dep-68ccc9c76f-f7rcn
5m15s       Normal    Scheduled              pod/wp-html-dep-68ccc9c76f-f7rcn     Successfully assigned wp-demo-ns/wp-html-dep-68ccc9c76f-f7rcn to k8s-vm3
5m15s       Normal    Scheduled              pod/wp-mysql-dep-678d654858-tc2l6    Successfully assigned wp-demo-ns/wp-mysql-dep-678d654858-tc2l6 to k8s-vm2
5m5s        Normal    Pulling                pod/wp-mysql-dep-678d654858-tc2l6    Pulling image "mysql:5.6"
5m1s        Normal    Pulling                pod/wp-html-dep-68ccc9c76f-f7rcn     Pulling image "wordpress:5-apache"
4m41s       Normal    Pulled                 pod/wp-mysql-dep-678d654858-tc2l6    Successfully pulled image "mysql:5.6" in 23.612866777s
4m34s       Normal    Created                pod/wp-mysql-dep-678d654858-tc2l6    Created container wp-mysql-con
4m34s       Normal    Started                pod/wp-mysql-dep-678d654858-tc2l6    Started container wp-mysql-con
4m12s       Normal    Pulled                 pod/wp-html-dep-68ccc9c76f-f7rcn     Successfully pulled image "wordpress:5-apache" in 48.887901336s
4m5s        Normal    Created                pod/wp-html-dep-68ccc9c76f-f7rcn     Created container wp-html-con
4m5s        Normal    Started                pod/wp-html-dep-68ccc9c76f-f7rcn     Started container wp-html-con
```

Notice - that eviction time was 5 minutes - which is actually fine (so network partition
caused for example by borked network switch upgrade should not invalidate cluster).

```bash
$ k-wp get deployment

NAME           READY   UP-TO-DATE   AVAILABLE   AGE
wp-mysql-dep   1/1     1            1           3h57m
wp-html-dep    1/1     1            1           102m
```

So deployments become READY=`1/1`. Tried reloading WordPress page in 
browser - and it worked - good!

And here is status of related Pods:

```bash
$ k-wp get pod -o wide

NAME                            READY   STATUS        RESTARTS   AGE     IP             NODE      NOMINATED NODE   READINESS GATES
wp-mysql-dep-678d654858-cdnfg   1/1     Terminating   0          3h59m   10.1.232.5     k8s-vm4   <none>           <none>
wp-html-dep-68ccc9c76f-smd7b    1/1     Terminating   0          104m    10.1.232.16    k8s-vm4   <none>           <none>
wp-mysql-dep-678d654858-tc2l6   1/1     Running       0          17m     10.1.145.244   k8s-vm2   <none>           <none>
wp-html-dep-68ccc9c76f-f7rcn    1/1     Running       0          17m     10.1.41.94     k8s-vm3   <none>           <none>
```

OK Time to put that node again Up! (and see if there is split-brain):

- after power up node was right-up:

  ```bash
  $ kubectl get node k8s-vm4

  NAME      STATUS   ROLES    AGE     VERSION
  k8s-vm4   Ready    <none>   6h19m   v1.22.2-3+9ad9ee77396805
  ```

- and listed Pods:

  ```bash
  $ k-wp get pod -o wide

  NAME                            READY   STATUS    RESTARTS   AGE   IP             NODE      NOMINATED NODE   READINESS GATES
  wp-mysql-dep-678d654858-tc2l6   1/1     Running   0          20m   10.1.145.244   k8s-vm2   <none>           <none>
  wp-html-dep-68ccc9c76f-f7rcn    1/1     Running   0          20m   10.1.41.94     k8s-vm3   <none>           <none>
  ```

- now we see that MySQL is running on `k8s-vm2` and Apache+PHP on `k8s-vm3`.


## Tips

YAML validation:

- to validate YAML files install (Ubuntu 20):

  ```bash
  sudo apt-get install yamllint
  ```

- and run:

  ```bash
  yamllint manifests/*.yaml
  ```

MarkDown validation:

- install

  ```bash
  sudo apt-get install ruby-dev
  sudo gem install mdl
  ```

- and run:

  ```bash
  mdl README.md
  ```

## Copyright

Here is (incomplete) list of used resources from other parties.

- Deploying Percona Single-Node to OpenEBS/Kubernetes:
  - https://openebs.io/docs/user-guides/jiva-guide#provision-sample-applications-with-jiva-volume

- MySQL Docker Image:
  - https://hub.docker.com/_/mysql/

- Using OpenSSL to generate password:
  - https://www.saotn.org/generate-random-passwords-openssl/

- Creating Secret from CLI:
  - https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/

- Using Secret to avoid exposure of MySQL root's password:
  - https://medium.com/@containerum/how-to-deploy-wordpress-and-mysql-on-kubernetes-bda9a3fdd2d5

- Another MySQL Deployment example:
  - https://www.redhat.com/en/blog/how-run-mysql-pod-ocp-using-ocs-and-statefulsets

